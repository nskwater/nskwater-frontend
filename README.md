Frontend of NSK Water ARM.

## Available Scripts
In the project directory, you can run:
### `REACT_APP_API_ROOT=http://localhost:5000/api npm start`
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`
Launches the test runner in the interactive watch mode.

### `npm run build`
Builds the app for production to the `build` folder.<br>
