import reducers from "./reducers"
import thunk from "redux-thunk"
import { apiMiddleware } from "redux-api-middleware"
import { applyMiddleware, createStore } from "redux"
import { composeWithDevTools } from "redux-devtools-extension/logOnlyInProduction"
import { createLogger } from "redux-logger"

const configureStore = () => {
  const middlewares = [thunk, apiMiddleware]

  if (process.env.NODE_ENV !== "production") {
    middlewares.push(createLogger())
  }

  const composeEnhancers = composeWithDevTools({})

  return createStore(
    reducers,
    composeEnhancers(applyMiddleware(...middlewares))
  )
}

export default configureStore
