import { api, authHeaders } from "./api"
import { CALL_API } from "redux-api-middleware"


export const LOGIN_REQUEST = "LOGIN_REQUEST"
export const LOGIN_SUCCESS = "LOGIN_SUCCESS"
export const LOGIN_FAILURE = "LOGIN_FAILURE"

const types = [
  LOGIN_REQUEST,
  {
    type: LOGIN_SUCCESS,
    payload: (action, state, res) =>
      res.json().then(json => Object.assign({}, json))
  },
  LOGIN_FAILURE
]

export const login = (username, password) => {
  const formData = new FormData()
  formData.append("identifier", username)
  formData.append("password", password)

  return {
    [CALL_API]: {
      endpoint: api("auth/login"),
      method: "POST",
      body: formData,
      types
    }
  }
}

export const fetchToken = () => ({
  [CALL_API]: {
    endpoint: api("private/token"),
    method: "GET",
    headers: authHeaders(),
    types
  }
})

export const logout = () => ({ type: LOGIN_FAILURE })
