export const OPEN_MODAL = "OPEN_MODAL"
export const CLOSE_MODAL = "CLOSE_MODAL"

export const closeModal = id => dispatch =>
  dispatch({
    type: CLOSE_MODAL,
    payload: { id }
  })

export const showModal = (id, props) => dispatch =>
  dispatch({
    type: OPEN_MODAL,
    payload: { id, props }
  })
