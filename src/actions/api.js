import { getAuthToken } from "reducers"

export const api = path => `${process.env.REACT_APP_API_ROOT}/${path}`

export const authHeaders = headers =>
  Object.assign({}, headers, { Authorization: `Bearer-${getAuthToken()}` })
