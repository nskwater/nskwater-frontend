import { api, authHeaders } from "./api"
import { CALL_API } from "redux-api-middleware"
import { normalize, schema } from "normalizr"

const portSchema = new schema.Entity("ports")
const arrayOfPorts = [portSchema]

export const PORT_REQUEST = "PORT_REQUEST"
export const PORT_SUCCESS = "PORT_SUCCESS"
export const PORT_FAILURE = "PORT_FAILURE"

export const fetchPorts = () => ({
  [CALL_API]: {
    endpoint: api("private/ports"),
    method: "GET",
    headers: authHeaders(),
    types: [
      PORT_REQUEST,
      {
        type: PORT_SUCCESS,
        payload: (action, state, res) =>
          res.json().then(json => normalize(json, arrayOfPorts))
      },
      PORT_FAILURE
    ]
  }
})
