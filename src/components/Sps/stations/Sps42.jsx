import * as S from 'components/scada'
import React from 'react'

export class Sps42 extends React.Component {
  tick = () => {
    const time = new Date().getTime()
    const dxTank1 = (5 - Math.floor(Math.random() * 10)) / 100
    const tank1 = this.state.tank1 + dxTank1
    const tankData1 = [...this.state.tankData1, { time, value: tank1 }]
    const pipe5speed = this.state.pipe5speed + dxTank1 / 2

    const dxTank2 = (5 - Math.floor(Math.random() * 10)) / 100
    const tank2 = this.state.tank2 + dxTank2
    const tankData2 = [...this.state.tankData2, { time, value: tank2 }]
    const pipe4speed = this.state.pipe4speed + dxTank2

    this.setState({
      tank1: tank1 > 4 ? 4 : tank1 < 0 ? 0 : tank1,
      tank2: tank2 > 8 ? 8 : tank2 < 0 ? 0 : tank2,
      pipe5speed: pipe5speed < 0 ? 0 : pipe5speed,
      pipe5volume: this.state.pipe5volume + pipe5speed / 2,
      pipe4speed: pipe4speed < 0 ? 0 : pipe4speed,
      pipe4volume: this.state.pipe4volume + pipe4speed,
      tankData1,
      tankData2
    })
    this.timeout = setTimeout(this.tick, 1000)
  }

  constructor(props) {
    super(props)
    this.state = {
      tank1: 2,
      tank2: 6.59,
      pipe5speed: 1,
      pipe5volume: 32,
      pipe4speed: 1.4,
      pipe4volume: 5,
      tankData1: [],
      tankData2: []
    }
  }

  componentDidMount() {
    this.tick()
  }

  componentWillUnmount() {
    clearTimeout(this.timeout)
  }

  render() {
    const { tankData1, tankData2 } = this.state
    const tankChart1 = tankData1.length > 40 ? tankData1.slice(tankData1.length - 40) : tankData1
    const tankChart2 = tankData2.length > 40 ? tankData2.slice(tankData2.length - 40) : tankData1
    return (
      <S.Svg viewWidth={1500} viewHeight={800}>
        <S.Pipe id="pipe1" path={[[20, 150], [300, 0]]} />
        <S.Pipe id="pipe2" path={[[320, 50], [0, 200], [500, 0], [0, 145]]} />
        <S.Pipe id="pipe3" path={[[820, 405], [0, 260]]} />
        <S.Pipe id="pipe4" path={[[430, 490], [0, 30], [390, 0], [0, 145], [-750, 0]]} animated />

        <S.Tank x={370} y={370} width={120} height={120} level={this.state.tank2 * 100 / 8} />
        <S.Label id="label3" x={430} y={360} value={this.state.tank2.toFixed(2)} label="м" />
        <S.Pipe id="pipe5" path={[[1330, 190], [0, 210], [-850, 0]]} animated />

        <S.Tank x={1270} y={70} width={120} height={120} level={this.state.tank1 * 100 / 4} />
        <S.Label id="label2" x={1330} y={60} value={this.state.tank1.toFixed(2)} label="м" />

        <S.Pipe id="pipe6" path={[[1480, 600], [0, -500], [-100, 0]]} animated />

        <S.Pump x={170} y={150} fill="#00FF00" />
        <S.Label id="label1" x={170} y={200} value="100" label="%" />

        <S.Label x={370} y={50} value="5.4" label="л/с" />
        <S.Label x={370} y={85} value="344.63" label="куб. м." />
        <S.Label x={370} y={120} value="1.1" label="бар" />

        {/* pipe4 */}
        <S.Label x={650} y={560} value={this.state.pipe4speed.toFixed(2)} label="л/с" />
        <S.Label x={650} y={595} value={this.state.pipe4volume.toFixed(2)} label="куб. м." />
        <S.Label x={650} y={630} value="2.87" label="бар" />

        {/* pipe5 */}
        <S.Label x={650} y={300} value={this.state.pipe5speed.toFixed(2)} label="л/с" />
        <S.Label x={650} y={335} value={this.state.pipe5volume.toFixed(2)} label="куб. м." />
        <S.Label x={650} y={370} value="3.1" label="бар" />

        <S.Pump x={320} y={200} rotate={-90} fill="#00FF00" />
        <S.Label x={380} y={200} value="100" label="%" />

        <S.Label x={880} y={245} value="0.0" label="л/с" />
        <S.Label x={880} y={280} value="253.01" label="куб. м." />
        <S.Pump x={820} y={320} rotate={-90} fill="#FF0000" />
        <S.Label id="label-crash" x={880} y={320} value="0" label="%" fill="#FF0000" />

        <S.TimeSeriesChart
          x={850}
          y={50}
          width={400}
          height={150}
          min={0}
          max={4}
          minLevel={1}
          maxLevel={3.5}
          chartData={tankChart1}
        />

        <S.Pump x={170} y={665} fill="#4444FF" />
        <S.Pump x={1330} y={250} rotate={-90} fill="#4444FF" />
      </S.Svg>
    )
  }
}
