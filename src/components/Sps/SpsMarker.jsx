import classNames from 'classnames'
import PropTypes from 'prop-types'
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import { Card, CardAction, CardActionIcons } from 'rmwc/Card'
import { divIcon } from 'leaflet'
import { ListDivider } from 'rmwc/List'
import { Marker, Popup } from 'react-leaflet'
import { Typography } from 'rmwc/Typography'
import { withRouter } from 'react-router'
import './SpsMarker.css'

const SpsMarkerBase = props => {
  const { number, x, y, crash, history } = props
  const position = [y, x]
  const onSpsClick = () => {
    history.push(`/sps/${number}`)
  }
  const animate = crash ? (
    <animate
      attributeType="XML"
      attributeName="fill"
      values="#800;#F00;#F88;#F00;#800"
      dur="1s"
      repeatCount="indefinite"
    />
  ) : (
    undefined
  )
  const circleFill = crash ? '#FF0000' : 'green'
  const svg = ReactDOMServer.renderToStaticMarkup(
    <svg className="sps-marker" width="32px" height="32px">
      <circle className={classNames('sps-marker__circle', { crash })} cx="16px" cy="16px" r="15px" fill={circleFill}>
        {animate}
      </circle>
      <text x="50%" y="50%" alignmentBaseline="middle" textAnchor="middle" fill="white">
        {number}
      </text>
    </svg>
  )
  const icon = divIcon({ html: svg })
  return (
    <Marker position={position} title={`КНС №${number}`} icon={icon}>
      <Popup className="sps-marker__popup" closeButton={false}>
        <Card>
          <Typography use="subheading1" tag="div" style={{ padding: '0 0.5rem' }}>
            {`КНС №${number}`}
          </Typography>
          <ListDivider />
          <CardActionIcons>
            <CardAction icon use="present_to_all" onClick={onSpsClick} />
          </CardActionIcons>
        </Card>
      </Popup>
    </Marker>
  )
}

SpsMarkerBase.propTypes = {
  number: PropTypes.number.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  crash: PropTypes.bool,
  history: PropTypes.object.isRequired
}

export const SpsMarker = withRouter(SpsMarkerBase)
