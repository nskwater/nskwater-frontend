import PropTypes from "prop-types"
import React from "react"
import { connect } from "react-redux"
import { isAuthenticated } from "reducers"
import { LayerGroup } from "react-leaflet"
import { SpsMarker } from "components"

class SpsLayerBase extends React.Component {
  componentDidMount() {
    const { isAuthenticated } = this.props
    if (isAuthenticated) {
    }
  }

  componentDidUpdate(prevProps) {
    const { isAuthenticated } = this.props
    if (isAuthenticated && prevProps.isAuthenticated !== isAuthenticated) {
    }
  }

  render() {
    return (
      <LayerGroup className="sps-layer">
        {this.props.sps.map((s, idx) => <SpsMarker key={idx} {...s} />)}
      </LayerGroup>
    )
  }
}

SpsLayerBase.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  sps: PropTypes.array
}

const mapStateToProps = state => ({
  isAuthenticated: isAuthenticated(state),
  sps: [{ number: 42, x: 82.9, y: 54.99, crash: true }, { number: 43, x: 82.955, y: 55.03 }]
})

export const SpsLayer = connect(mapStateToProps)(SpsLayerBase)
