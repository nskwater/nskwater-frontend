import * as S from './stations'
import React from 'react'

export const SpsForm = props => {
  if (props.match.params.id === "42") {
    return <S.Sps42 />
  } else if (props.match.params.id === "43") {
    return <S.Sps43 />
  }
  return <p>{`SpsForm: ${props.match.params.id}`}</p>
}
