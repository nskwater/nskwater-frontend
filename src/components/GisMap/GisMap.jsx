import classNames from "classnames"
import PropTypes from "prop-types"
import React from "react"
import { LayersControl, Map as LeafletMap, TileLayer } from "react-leaflet"
import { PortsLayer, SpsLayer } from "components"
import "./GisMap.css"

const position = [55.01, 82.915]
const { BaseLayer, Overlay } = LayersControl

export const GisMap = props => (
  <div className={classNames("gis-map", props.className)}>
    <LeafletMap center={position} zoom={13}>
      <LayersControl position="topright">
        <BaseLayer checked name="Cхема">
          <TileLayer
            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
            url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
          />
        </BaseLayer>
        <BaseLayer name="Спутник">
          <TileLayer url="https://api.mapbox.com/styles/v1/redup/ciu8izpyq003l2inx0e73deer/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoicmVkdXAiLCJhIjoiY2l1OGl5ZjR3MDAxMzJ6bzU0ZDV6aGtzZyJ9.i9FBxHrsRyf3bINlPgXaaA" />
        </BaseLayer>
        <Overlay className="ports-overlay" checked name="Водопроводные камеры">
          <PortsLayer />
        </Overlay>
        <Overlay checked name="КНС">
          <SpsLayer />
        </Overlay>
      </LayersControl>
    </LeafletMap>
  </div>
)

GisMap.propTypes = {
  className: PropTypes.string
}
