import * as actionsModal from "actions/modal"
import PropTypes from "prop-types"
import React from "react"
import { connect } from "react-redux"
import { getModals } from "reducers"
import { Portal } from "react-portal"
import { SimpleDialog } from "rmwc/Dialog"

export class Modals extends React.Component {
  propsToState = props => ({
    modals: props.modals.map((item, i) => {
      const { id, props } = item
      return (
        <div key={i}>
          <SimpleDialog
            {...props}
            key={i}
            open={true}
            onClose={() => this.props.closeModal(id)}
          />
        </div>
      )
    })
  })

  constructor(props) {
    super(props)
    this.state = this.propsToState(props)
  }

  componentWillReceiveProps(next) {
    this.setState(this.propsToState(next))
  }

  shouldComponentUpdate(next) {
    return JSON.stringify(this.props.modals) !== JSON.stringify(next.modals)
  }

  render() {
    return <Portal id="portal-id">{this.state.modals}</Portal>
  }
}

Modals.propTypes = {
  modals: PropTypes.array.isRequired,
  closeModal: PropTypes.func.isRequired
}

export const ModalsContainer = connect(
  state => ({
    modals: getModals(state)
  }),
  actionsModal
)(Modals)
