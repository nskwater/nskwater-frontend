import classNames from "classnames"
import PropTypes from "prop-types"
import React from "react"
import {
  Toolbar,
  ToolbarFixedAdjust,
  ToolbarIcon,
  ToolbarRow,
  ToolbarSection,
  ToolbarTitle
} from "rmwc/Toolbar"

export const Header = props => (
  <div className={classNames("header", props.className)}>
    <Toolbar fixed>
      <ToolbarRow>
        <ToolbarSection alignStart>
          {props.goBack !== undefined ? <ToolbarIcon use="arrow_back" onClick={props.goBack} /> : <span/>}
          <ToolbarTitle className="header__title">Горводоканал</ToolbarTitle>
        </ToolbarSection>
        <ToolbarSection alignEnd>
          <ToolbarIcon use="power_settings_new" onClick={props.onExit} />
        </ToolbarSection>
      </ToolbarRow>
    </Toolbar>
    <ToolbarFixedAdjust />
  </div>
)

Header.propTypes = {
  className: PropTypes.string,
  onExit: PropTypes.func,
  goBack: PropTypes.func
}
