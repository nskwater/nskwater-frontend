import React from "react"

/*
const routes = ({store}) => {
    const requireAuth = (nextState, replace) => {
        const { pathname } = nextState.location
        const isAuth = isAuthenticated(store.getState())

        if (!isAuth && pathname !== '/login') {
            replace({
                pathname: '/login',
                state: { nextPathname: pathname }
            })
        }
    }

    return (
        <Route path={basePath} component={App} onEnter={requireAuth}>
            <Route name="login" path="login" component={LoginPage}/>
            <Route name="gismap" path="gismap" component={GisMap}/>
            <Route name="notfound" path="*" component={NotFound} status={404}/>
        </Route>
    )
}

*/
/*
const PrivateRouteBase = ({ component: Component, isAuthenticated, ...rest }) => (
  <Route {...rest} render={ props => isAuthenticated
      ? <Component {...props} />
      : <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
  }/>
)

PrivateRouteBase.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired
}

const PrivateRoute = withRouter(connect(
  (state) => ({
    isAuthenticated: isAuthenticated(state)
  })
)(PrivateRouteBase))
*/

const Routes = () => (
  <div/>
)

export default Routes
