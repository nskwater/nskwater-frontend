import * as actionsAuth from "actions/auth"
import classNames from "classnames"
import PropTypes from "prop-types"
import React from "react"
import { connect } from "react-redux"
import { getAuthToken, isAuthenticated, isAuthTokenFetching } from "reducers"
import { TextField } from "rmwc/TextField"
import { Button } from "rmwc/Button"
import { Redirect, withRouter } from "react-router"
import "./LoginForm.css"

class LoginFormBase extends React.Component {
  componentWillMount() {
    const { isAuthenticated, authToken, fetchToken } = this.props
    if (!isAuthenticated && authToken) {
      fetchToken()
    }
  }

  onSubmit = event => {
    event.preventDefault()
    this.props.login(this.userRef.value, this.passwordRef.value)
  }

  render() {
    const { isAuthTokenFetching, isAuthenticated } = this.props
    const { from } = this.props.location.state || { from: { pathname: "/" } }

    if (isAuthenticated) {
      return <Redirect to={from} />
    }

    const loginForm = (
      <div className="login">
        <form
          className={classNames("login__form", this.props.className)}
          onSubmit={this.onSubmit}
        >
          <TextField
            outlined
            label="Пользователь"
            inputRef={el => (this.userRef = el)}
          />
          <TextField
            outlined
            type="password"
            label="Пароль"
            inputRef={el => (this.passwordRef = el)}
          />
          <Button type="submit" raised>
            Вход
          </Button>
        </form>
      </div>
    )
    return isAuthTokenFetching ? <span>Loading...</span> : loginForm
  }
}

LoginFormBase.contextTypes = {
  router: PropTypes.object.isRequired
}

LoginFormBase.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  isAuthTokenFetching: PropTypes.bool.isRequired,
  login: PropTypes.func.isRequired,
  fetchToken: PropTypes.func.isRequired,
  authToken: PropTypes.string,
  className: PropTypes.string
}

const mapStateToProps = state => ({
  isAuthenticated: isAuthenticated(state),
  authToken: getAuthToken(),
  isAuthTokenFetching: isAuthTokenFetching(state)
})

export const LoginForm = withRouter(connect(mapStateToProps, actionsAuth)(LoginFormBase))
