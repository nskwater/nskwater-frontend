import * as actionsModal from 'actions/modal'
import _ from 'lodash'
import PropTypes from 'prop-types'
import React from 'react'
import ReactHighstock from 'react-highcharts/dist/ReactHighstock'
import { Card, CardAction, CardActions, CardPrimaryAction } from 'rmwc/Card'
import { connect } from 'react-redux'
import { ListDivider } from 'rmwc/List'
import { Popup } from 'react-leaflet'
import { Typography } from 'rmwc/Typography'
import './PortPopup.css'

const getData = () => {
  let date = Date.parse('2016-10-10 00:00:00')
  const endDate = Date.parse('2016-10-10 23:00:00')
  let value = 3

  const result = []
  while (date < endDate) {
    const multiple = _.random(-1, 1)
    value += multiple * _.random(0, 1, true) * 0.1
    if (value > 8) {
      value = 8
    }
    if (value < 0) {
      value = 0
    }
    result.push([date, value])

    date += 60000
  }

  return result
}

const config = {
  rangeSelector: {
    selected: 1
  },
  series: [
    {
      name: 'Давление',
      data: getData(),
      tooltip: {
        valueDecimals: 2
      }
    }
  ],
  xAxis: {
    type: 'datetime',
    title: {
      text: 'Date'
    }
  }
}

const item = data => (data ? <div className="card-list__item">{data}</div> : <span />)

const PortPopup = ({ port, showModal }) => {
  const { tech_nomer, street, num_house, raspolog, note } = port
  if (!tech_nomer && !street && !num_house && !raspolog && !note) {
    return <span />
  }
  const streetContainer = street ? street + ' ' + num_house : undefined
  const chartClick = () =>
    showModal(port.id, {
      title: tech_nomer,
      acceptLabel: null,
      body: <ReactHighstock config={config} />
    })
  const content = (
    <Card>
      <Typography use="subheading1" tag="div" style={{ padding: '0 0.5rem' }}>
        {tech_nomer}
      </Typography>
      <ListDivider />
      <CardPrimaryAction>
        <Typography use="body1" tag="div" className="port-popup__list card-list">
          {item(streetContainer)}
          {item(raspolog)}
          {item(note)}
        </Typography>
      </CardPrimaryAction>
      <ListDivider />
      <CardActions>
        <CardAction onClick={chartClick}>График давления</CardAction>
      </CardActions>
    </Card>
  )
  return (
    <Popup className="port-popup" closeButton={false}>
      {content}
    </Popup>
  )
}

PortPopup.propTypes = {
  port: PropTypes.object.isRequired,
  showModal: PropTypes.func.isRequired
}

export default connect(null, actionsModal)(PortPopup)
