import * as actionsPorts from "actions/ports"
import MarkerClusterGroup from "react-leaflet-markercluster"
import PortMarker from "./PortMarker"
import PropTypes from "prop-types"
import React from "react"
import { connect } from "react-redux"
import { getPorts, isAuthenticated } from "reducers"

class PortsLayer extends React.Component {
  propsToState = props => ({
    markers: Object.values(props.ports).map((port, idx) => (
      <div key={idx}>
        <PortMarker port={port} />
      </div>
    ))
  })

  constructor(props) {
    super(props)
    this.state = this.propsToState(props)
  }

  componentWillReceiveProps(next) {
    this.setState(this.propsToState(next))
  }

  componentDidMount() {
    const { isAuthenticated, fetchPorts } = this.props
    if (isAuthenticated) {
      fetchPorts()
    }
  }

  componentDidUpdate(prevProps) {
    const { isAuthenticated, fetchPorts } = this.props
    if (isAuthenticated && prevProps.isAuthenticated !== isAuthenticated) {
      fetchPorts()
    }
  }

  render() {
    return (
      <MarkerClusterGroup className="ports-layer" spiderfyOnMaxZoom={false} disableClusteringAtZoom={16}>
        {this.state.markers}
      </MarkerClusterGroup>
    )
  }
}

PortsLayer.propTypes = {
  ports: PropTypes.array.isRequired,
  isAuthenticated: PropTypes.bool.isRequired
}

const mapStateToProps = state => ({
  ports: getPorts(state),
  isAuthenticated: isAuthenticated(state)
})

export default connect(mapStateToProps, actionsPorts)(PortsLayer)
