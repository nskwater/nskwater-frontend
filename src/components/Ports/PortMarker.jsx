import PortPopup from "./PortPopup/PortPopup"
import PropTypes from "prop-types"
import React from "react"
import { Marker } from "react-leaflet"

const center = coordinatesString => {
  const coordinates = coordinatesString.split(" ").map(parseFloat)
  return [
    coordinates.reduce((total, curr, i) => (i % 2 !== 0 ? total + curr : total), 0) / (coordinates.length / 2),
    coordinates.reduce((total, curr, i) => (i % 2 === 0 ? total + curr : total), 0) / (coordinates.length / 2)
  ]
}

const PortMarker = ({ port }) => (
  <Marker position={center(port.coordinates)} title={port.tech_nomer}>
    <PortPopup port={port} />
  </Marker>
)

PortMarker.propTypes = {
  port: PropTypes.object.isRequired
}

export default PortMarker
