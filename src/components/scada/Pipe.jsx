// @flow
import * as React from 'react'
import PropTypes from 'prop-types'

export class Pipe extends React.Component {
  render() {
    const props = this.props
    const d = props.path.map(p => `${p[0]},${p[1]}`).join('l')
    const dur = props.path.filter((_, idx) => idx > 0).reduce((p, c) => p + Math.abs(c[0]) + Math.abs(c[1]) , 0) / 100
    const animated = props.animated ? (
      <circle r="8" fill="#4444FF">
        <animateMotion dur={`${dur}s`} repeatCount="indefinite">
          <mpath xlinkHref={`#${props.id}`} />
        </animateMotion>
      </circle>
    ) : (
      <span />
    )

    return (
      <g>
        <path d={`m${d}`} fill="none" stroke="#4444FF" strokeWidth="4" strokeLinejoin="round" id={props.id} />
        {animated}
      </g>
    )
  }
}

Pipe.propTypes = {
  path: PropTypes.array.isRequired,
  fill: PropTypes.string,
  stroke: PropTypes.string,
  id: PropTypes.string,
  animated: PropTypes.bool
}
