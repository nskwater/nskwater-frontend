import * as React from 'react'
import PropTypes from 'prop-types'

export class Tank extends React.Component {
  static propTypes = {
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    level: PropTypes.number.isRequired,
    width: PropTypes.number,
    height: PropTypes.number,
    className: PropTypes.string  
  }
  
  render() {
    const { x, y, level, width = 80, height = 100 } = this.props
    return (
      <g className="tank">
        <path
          fill="#4444FF"
          stroke="black"
          strokeWidth={1}
          strokeLinecap="round"
          strokeLinejoin="round"
          d={`m${x},${y + (1 - level / 100) * height}l${width},0l0,${height * level / 100}l${-width},0z`}
        />
        <path
          d={`m${x},${y}l0,${height}l${width},0l0,${-height}`}
          strokeLinejoin="round"
          strokeWidth="4"
          stroke="#8F8F8F"
          fill="none"
        />
      </g>
    )
  }
}
