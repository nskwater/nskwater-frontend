import * as React from 'react'
import PropTypes from 'prop-types'

export class Pump extends React.Component {
  static propTypes = {
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    size: PropTypes.number,
    rotate: PropTypes.number
  }

  render() {
    const { x = 0, y = 0, size = 40, fill = '#CCCCCC', stroke = 'gray', strokeWidth = 1, rotate = 0 } = this.props
    const s = (q) => q * size
    const props = { strokeWidth, stroke, fill }
    return (
      <g transform={`rotate(${rotate} ${x} ${y})`}>
        <line {...props} x1={x} y1={y} x2={x} y2={y - s(0.5)} />
        <rect {...props} x={x - s(0.375)} y={y - s(0.125)} width={size - s(0.25)} height={s(0.25)} />
        <rect {...props} x={x - s(0.5)} y={y - s(0.25)} width={s(0.125)} height={s(0.5)} />
        <rect {...props} x={x + s(0.375)} y={y - s(0.25)} width={s(0.125)} height={s(0.5)} />
        <circle {...props} cx={x} cy={y} r={s(0.25)} />
        <circle {...props} cx={x} cy={y - s(0.5)} r={s(.2)} />
      </g>
    )
  }
}
