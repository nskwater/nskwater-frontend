// @flow
import * as React from 'react'
import PropTypes from 'prop-types'

export class Label extends React.Component {
  render() {
    const { id, x, y, height = 30, width = 160, fill = "#FFFFFF" } = this.props
    const h = q => height * q
    const w = q => width * q
    return (
      <g>
        <svg id={id} x={x - w(.25)} y={y - h(0.5)} height={h(1)} width={w(1)}>
          <rect width="50%" height="100%" fill={fill} />
          <text x="25%" y="50%" alignmentBaseline="middle" textAnchor="middle" fontSize="20">
            {this.props.value}
          </text>
          <text x="55%" y="50%" width="50%" alignmentBaseline="middle" textAnchor="left" fontSize="15">
            {this.props.label}
          </text>
        </svg>
      </g>
    )
  }
}

Label.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  label: PropTypes.string,
  value: PropTypes.string,
  height: PropTypes.number,
  width: PropTypes.number,
  id: PropTypes.string,
  fill: PropTypes.string
}
