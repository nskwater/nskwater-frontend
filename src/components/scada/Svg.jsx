import * as React from 'react'
import PropTypes from 'prop-types'
import './Svg.css'

export const Svg = props => (
  <svg className="svg-container" viewBox={`0 0 ${props.viewWidth} ${props.viewHeight}`} >
    {props.children}
  </svg>
)

Svg.propTypes = {
  viewWidth: PropTypes.number.isRequired,
  viewHeight: PropTypes.number.isRequired
}