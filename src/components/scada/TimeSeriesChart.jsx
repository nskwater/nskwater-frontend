import PropTypes from 'prop-types'
import React from 'react'
import { CartesianGrid, ReferenceLine, Scatter, ScatterChart, Tooltip, XAxis, YAxis } from 'recharts'

export const TimeSeriesChart = ({ chartData, x, y, width, height, min, max, minLevel, maxLevel }) => (
  <ScatterChart compact x={x} y={y} width={width} height={height}>
    <XAxis
      dataKey="time"
      domain={[
        chartData.length > 0 ? chartData[0].time : 'auto',
        chartData.length > 0 ? chartData[chartData.length - 1].time : 'auto'
      ]}
      tickFormatter={date => new Date().toLocaleTimeString()}
      type="number"
    />
    <YAxis dataKey="value" orientation="right" domain={[min | 'auto', max | 'auto']} />
    <CartesianGrid strokeDasharray="3 3" />
    <Tooltip cursor={{ strokeDasharray: '3 3' }} />
    {maxLevel ? <ReferenceLine y={maxLevel} stroke="red" /> : undefined}
    {minLevel ? <ReferenceLine y={minLevel} stroke="red" /> : undefined}
    <Scatter legendType="none" data={chartData} line={{ stroke: '#FFF' }} lineJointType="monotoneX" lineType="joint" />
  </ScatterChart>
)

TimeSeriesChart.propTypes = {
  chartData: PropTypes.arrayOf(
    PropTypes.shape({
      time: PropTypes.number,
      value: PropTypes.number
    })
  ).isRequired,
  x: PropTypes.number,
  y: PropTypes.number,
  width: PropTypes.number,
  height: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  minLevel: PropTypes.number,
  maxLevel: PropTypes.number
}
