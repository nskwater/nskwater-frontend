import * as React from 'react'
import PropTypes from 'prop-types'

export class Valve extends React.Component {
  static propTypes = {
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    width: PropTypes.number,
    height: PropTypes.number,
    rotate: PropTypes.number,
    fill: PropTypes.string,
    stroke: PropTypes.string,
    strokeWidth: PropTypes.number
  }
  render() {
    const { x = 0, y = 0, width = 100, height = 100, rotate = 0 } = this.props
    const { stroke = 'gray', fill = '#CCCCCC', strokeWidth = 1 } = this.props
    const props = { strokeWidth, stroke, fill }
    const w = (q) => width * q
    const h = (q) => height * q
    return (
      <g transform={`rotate(${rotate} ${x} ${y})`}>
        <path
          {...props}
          id={this.props.id}
          d={`m${x - w(0.5)}, ${y - h(0.5)}
              l${width},${height}
              l0, ${-height}
              l${-width}, ${height}
              l0,${-height}z`}
          strokeLinejoin="round"
        />
        <line {...props} x1={x} y1={y} x2={x} y2={y - h(0.5)} />
        <rect {...props} x={x - w(0.25)} y={y - h(0.9)} width={w(0.5)} height={h(0.5)} />
      </g>
    )
  }
}
