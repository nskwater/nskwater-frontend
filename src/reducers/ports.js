import * as actionsPorts from "actions/ports"
import { combineReducers } from "redux"

const ports = (state = [], { type, payload }) => {
  switch (type) {
    case actionsPorts.PORT_SUCCESS:
      return payload.entities.ports ? payload.entities.ports : state 
    default:
      return state
  }
}

const isFetchingPorts = (state = false, { type }) => {
  switch (type) {
    case actionsPorts.PORT_REQUEST:
      return true
    case actionsPorts.PORT_SUCCESS:
    case actionsPorts.PORT_FAILURE:
      return false
    default:
      return state
  }
}

export default combineReducers({
  ports,
  isFetchingPorts
})

export const getPorts = state => state.ports.ports
