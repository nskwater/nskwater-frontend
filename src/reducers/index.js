import { combineReducers } from "redux"
import { routerReducer } from "react-router-redux"
import auth, * as fromAuth from "./auth"
import ports, * as fromPorts from "./ports"
import modals, * as fromModals from "./modal"

const reducers = combineReducers({
  routing: routerReducer,
  auth,
  modals,
  ports
})

export default reducers

export const getAuthToken = () => fromAuth.getAuthToken()
export const isAuthTokenFetching = state => fromAuth.isFetching(state)
export const getUserName = state => fromAuth.getUserName(state)
export const isAuthenticated = state => fromAuth.isAuthenticated(state)

export const getPorts = state => fromPorts.getPorts(state)
export const getModals = state => fromModals.getModals(state)
