import * as actions from "actions/modal"

const initialState = []

const modal = (state = initialState, { type, payload }) => {
  switch (type) {
    case actions.OPEN_MODAL:
      return [...state, payload]
    case actions.CLOSE_MODAL:
      return [...state.filter(item => item.id !== payload.id)]
    default:
      return state
  }
}

export default modal

export const getModals = state => state.modals
