import * as actionsAuth from "actions/auth"

const TOKEN = "auth-token"
const setAuthToken = token => localStorage.setItem(TOKEN, token)
const removeAuthToken = () => localStorage.removeItem(TOKEN)
export const getAuthToken = () => localStorage.getItem(TOKEN)

const initialState = {
  username: null,
  isAuthenticated: false,
  isFetching: false
}

const auth = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionsAuth.LOGIN_SUCCESS: {
      const { token, username } = payload
      setAuthToken(token)
      return Object.assign({}, state, {
        username,
        isFetching: false,
        isAuthenticated: true
      })
    }
    case actionsAuth.LOGIN_REQUEST: {
      removeAuthToken()
      return Object.assign({}, state, {
        isFetching: true,
        isAuthenticated: false
      })
    }
    case actionsAuth.LOGIN_FAILURE: {
      removeAuthToken()
      return Object.assign({}, initialState)
    }
    default:
      return state
  }
}

export default auth

export const isAuthenticated = state => state.auth.isAuthenticated
export const getUserName = state => state.auth.username
export const isFetching = state => state.auth.isFetching
