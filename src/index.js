import * as C from 'components'
import configureStore from './configureStore'
import L from 'leaflet'
import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Redirect, Route, withRouter } from 'react-router-dom'
import { isAuthenticated } from 'reducers'
import { logout } from 'actions/auth'
import { Provider } from 'react-redux'
import './index.css'

L.Icon.Default.imagePath = process.env.PUBLIC_URL + '/images/'

const store = configureStore()

console.log('The backend address: ' + process.env.REACT_APP_API_ROOT)

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      const auth = isAuthenticated(store.getState())
      return auth ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: '/login',
            state: { from: props.location }
          }}
        />
      )
    }}
  />
)

const RouterHeader = withRouter(({ history, location }) => {
  const exit = () => {
    store.dispatch(logout())
    history.push('/')
  }
  const back = location.pathname.indexOf('/sps/') !== 0 ? undefined : () => {
    history.goBack()
  }
  return isAuthenticated(store.getState()) ? <C.Header className="app__header" onExit={exit} goBack={back} /> : <span />
})

const gisMapRoute = () => <C.GisMap className="app__route" />
const loginRoute = () => <C.LoginForm className="app__route" />
const spsFormRoute = props => <C.SpsForm className="app__route" {...props} />

ReactDOM.render(
  <Provider store={store} key="provider">
    <BrowserRouter>
      <React.Fragment>
        <RouterHeader />
        <PrivateRoute exact path="/" component={gisMapRoute} />
        <PrivateRoute exact path="/sps/:id" component={spsFormRoute} />
        <Route path="/login" render={loginRoute} />
        <C.ModalsContainer />
      </React.Fragment>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)
